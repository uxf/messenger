<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Request;

final class ThreadsRequestQuery
{
    public function __construct(
        public readonly ?int $limit = null,
        public readonly ?int $offset = null,
    ) {
    }
}
