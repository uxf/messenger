<?php

declare(strict_types=1);

namespace UXF\MessengerTests\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Shared\Entity\ProfileInterface;

#[ORM\Entity]
class FakeProfile implements ProfileInterface
{
    public const JOHN_ID = 1;
    public const EMMA_ID = 2;

    #[ORM\Column, ORM\Id]
    private int $id;

    #[ORM\Column]
    private string $fullName;

    public function __construct(int $id, string $fullName)
    {
        $this->id = $id;
        $this->fullName = $fullName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }
}
