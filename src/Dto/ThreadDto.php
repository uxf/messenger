<?php

declare(strict_types=1);

namespace UXF\Messenger\Dto;

use UXF\Messenger\Entity\Message;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Http\Response\ProfileInteractionResponse;

class ThreadDto
{
    /**
     * @param ProfileInteractionResponse[] $profileInteractions
     */
    public function __construct(
        public readonly Thread $thread,
        public readonly array $profileInteractions,
        public readonly ?Message $lastMessage,
        public readonly ?int $unreadMessageCount,
    ) {
    }
}
