<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Request;

final class ThreadRequestBody
{
    /**
     * @param int[] $profiles
     */
    public function __construct(
        public readonly string $name,
        public readonly array $profiles = [],
    ) {
    }
}
