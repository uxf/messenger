<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Request;

final class MessageRequestQuery
{
    public function __construct(
        public readonly ?int $cursor = null,
    ) {
    }
}
