<?php

declare(strict_types=1);

namespace UXF\Messenger;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UXFMessengerBundle extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
