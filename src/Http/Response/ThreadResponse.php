<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Response;

use UXF\Messenger\Dto\ThreadDto;

final class ThreadResponse
{
    /**
     * @param ProfileInteractionResponse[] $profileInteractions
     */
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly ?MessageResponse $lastMessage,
        public readonly array $profileInteractions,
        public readonly ?int $unreadMessageCount,
    ) {
    }

    public static function createFromDto(ThreadDto $dto): self
    {
        return new self(
            $dto->thread->getId(),
            $dto->thread->getName(),
            MessageResponse::createNullable($dto->lastMessage),
            $dto->profileInteractions,
            $dto->unreadMessageCount,
        );
    }
}
