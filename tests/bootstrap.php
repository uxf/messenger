<?php

declare(strict_types=1);

use UXF\Core\SystemProvider\Uuid;

require __DIR__ . '/../../../vendor/autoload.php';

Uuid::$seq = 1;
