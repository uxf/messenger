<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Response;

use UXF\Core\Type\DateTime;
use UXF\Messenger\Entity\Message;
use UXF\Storage\Http\Response\FileResponse;

final class MessageResponse
{
    public function __construct(
        public readonly int $id,
        public readonly ?ProfileResponse $author,
        public readonly string $text,
        public readonly ?FileResponse $file,
        public readonly DateTime $createdAt,
    ) {
    }

    public static function create(Message $message): self
    {
        return new self(
            $message->getId(),
            ProfileResponse::create($message->getAuthor()),
            $message->getText(),
            FileResponse::createNullable($message->getFile()),
            $message->getCreatedAt(),
        );
    }

    public static function createNullable(?Message $message): ?self
    {
        return $message !== null ? self::create($message) : null;
    }
}
