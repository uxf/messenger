<?php

declare(strict_types=1);

namespace UXF\MessengerTests\Story;

use Nette\Utils\Json;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Core\Test\Client;
use UXF\Core\Test\WebTestCase;
use UXF\MessengerTests\Entity\FakeProfile;
use UXF\MessengerTests\Mock\FakeCurrentProfileProvider;

use function Safe\sleep;

class CommunicationStoryTest extends WebTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->post('/api/messenger', ['name' => 'Tinder', 'profiles' => [FakeProfile::EMMA_ID]]);
        self::assertResponseIsSuccessful();

        self::assertSame([
            'id' => 1,
            'name' => 'Tinder',
            'lastMessage' => null,
            'profileInteractions' => [
                [
                    'id' => '1_1',
                    'profile' => [
                        'id' => 1,
                        'fullName' => 'John Doe',
                    ],
                    'lastReadMessageId' => null,
                ],
                [
                    'id' => '1_2',
                    'profile' => [
                        'id' => 2,
                        'fullName' => 'Emma Watson',
                    ],
                    'lastReadMessageId' => null,
                ],
            ],
            'unreadMessageCount' => 0,
        ], $client->getResponseData());

        $this->setAuthor($client, FakeProfile::JOHN_ID);
        (new Filesystem())->copy(__DIR__ . '/../file/x.png', __DIR__ . '/../public/x.png');
        $client->request(
            'POST',
            '/api/messenger/1/message',
            [],
            ['file' => new UploadedFile(__DIR__ . '/../public/x.png', 'x.png', 'image/png', null, true)],
            [],
            Json::encode(['text' => 'What\'s up? ❤️'])
        );

        $this->setAuthor($client, FakeProfile::EMMA_ID);
        $client->post('/api/messenger/1/message', ['text' => 'Fuck you!']);

        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->post('/api/messenger/1/message', ['text' => 'Are you kidding me?']);

        $this->setAuthor($client, FakeProfile::EMMA_ID);
        $client->post('/api/messenger/1/message', ['text' => '🍑']);
        $client->post('/api/messenger/1/message', ['text' => '🍍']);

        // john read message 5
        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->get('/api/messenger/1/message');

        $this->setAuthor($client, FakeProfile::EMMA_ID);
        $client->post('/api/messenger/1/message', ['text' => '🍿']);
        $client->post('/api/messenger/1/message', ['text' => '🎂']);

        $client->get('/api/messenger/1/message');

        $data = $client->getResponseData();
        array_walk($data, static function (&$item): void {
            $item['createdAt'] = '';
        });

        self::assertSame([
            [
                'id' => 7,
                'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                'text' => '🎂',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 6,
                'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                'text' => '🍿',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 5,
                'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                'text' => '🍍',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 4,
                'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                'text' => '🍑',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 3,
                'author' => ['id' => 1, 'fullName' => 'John Doe'],
                'text' => 'Are you kidding me?',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 2,
                'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                'text' => 'Fuck you!',
                'file' => null,
                'createdAt' => '',
            ],
            [
                'id' => 1,
                'author' => ['id' => 1, 'fullName' => 'John Doe'],
                'text' => 'What\'s up? ❤️',
                'file' => [
                    'id' => 1,
                    'uuid' => '00000000-0000-0000-0000-000000000001',
                    'type' => 'image/png',
                    'extension' => 'png',
                    'name' => 'x.png',
                    'namespace' => 'messenger',
                ],
                'createdAt' => '',
            ],
        ], $data);

        // create second thread
        sleep(1); // hack sqlite datetime
        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->post('/api/messenger', ['name' => 'Tinder 2']);

        // create third thread
        sleep(1); // hack sqlite datetime
        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->post('/api/messenger', ['name' => 'Tinder 3', 'profiles' => [FakeProfile::EMMA_ID]]);

        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->post('/api/messenger/2/message', ['text' => 'Last man standing!']);

        $this->setAuthor($client, FakeProfile::JOHN_ID);
        $client->get('/api/messenger');
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        array_walk($data, static function (&$item): void {
            if (isset($item['lastMessage'])) {
                $item['lastMessage']['createdAt'] = '';
            }
        });

        self::assertSame([
            [
                'id' => 2,
                'name' => 'Tinder 2',
                'lastMessage' => [
                    'id' => 8,
                    'author' => ['id' => 1, 'fullName' => 'John Doe'],
                    'text' => 'Last man standing!',
                    'file' => null,
                    'createdAt' => '',
                ],
                'profileInteractions' => [
                    [
                        'id' => '2_1',
                        'profile' => ['id' => 1, 'fullName' => 'John Doe'],
                        'lastReadMessageId' => 8,
                    ],
                ],
                'unreadMessageCount' => 0,
            ],
            [
                'id' => 3,
                'name' => 'Tinder 3',
                'lastMessage' => null,
                'profileInteractions' => [
                    [
                        'id' => '3_1',
                        'profile' => ['id' => 1, 'fullName' => 'John Doe'],
                        'lastReadMessageId' => null,
                    ],
                    [
                        'id' => '3_2',
                        'profile' => ['id' => 2, 'fullName' => 'Emma Watson'],
                        'lastReadMessageId' => null,
                    ],
                ],
                'unreadMessageCount' => 0,
            ],
            [
                'id' => 1,
                'name' => 'Tinder',
                'lastMessage' => [
                    'id' => 7,
                    'author' => ['id' => 2, 'fullName' => 'Emma Watson'],
                    'text' => '🎂',
                    'file' => null,
                    'createdAt' => '',
                ],
                'profileInteractions' => [
                    [
                        'id' => '1_1',
                        'profile' => ['id' => 1, 'fullName' => 'John Doe'],
                        'lastReadMessageId' => 5,
                    ],
                    [
                        'id' => '1_2',
                        'profile' => ['id' => 2, 'fullName' => 'Emma Watson'],
                        'lastReadMessageId' => 7,
                    ],
                ],
                'unreadMessageCount' => 2,
            ],
        ], $data);
    }

    private function setAuthor(Client $client, int $profileId): void
    {
        self::$kernel->shutdown();
        self::$kernel->boot();
        $client->disableReboot();

        /** @var FakeCurrentProfileProvider $profileProvider */
        $profileProvider = self::getContainer()->get(CurrentProfileProviderInterface::class);
        $profileProvider->profileId = $profileId;
    }
}
