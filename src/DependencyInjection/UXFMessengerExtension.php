<?php

declare(strict_types=1);

namespace UXF\Messenger\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use UXF\Core\Shared\Entity\ProfileInterface;

class UXFMessengerExtension extends Extension implements PrependExtensionInterface
{
    /**
     * @param mixed[] $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->import('services.php');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     */
    public function prepend(ContainerBuilder $container): void
    {
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);

        // doctrine
        $container->prependExtensionConfig('doctrine', [
            'orm' => [
                'mappings' => [
                    'UXFMessenger' => [
                        'type' => 'attribute',
                        'dir' => __DIR__ . '/../Entity',
                        'prefix' => 'UXF\Messenger\Entity',
                    ],
                ],
                'resolve_target_entities' => [
                    ProfileInterface::class => $config['profile_class'],
                ]
            ],
        ]);
    }
}
