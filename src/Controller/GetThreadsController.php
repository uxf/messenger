<?php

declare(strict_types=1);

namespace UXF\Messenger\Controller;

use UXF\Core\Http\Request\FromQuery;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Messenger\Dto\ThreadDto;
use UXF\Messenger\Facade\ReadThreadFacade;
use UXF\Messenger\Http\Request\ThreadsRequestQuery;
use UXF\Messenger\Http\Response\ThreadResponse;

class GetThreadsController
{
    public function __construct(
        private readonly ReadThreadFacade $threadFacade,
        private readonly CurrentProfileProviderInterface $currentProfileProvider,
    ) {
    }

    /**
     * @return ThreadResponse[]
     */
    public function __invoke(#[FromQuery] ThreadsRequestQuery $query): array
    {
        $profile = $this->currentProfileProvider->getProfile();
        return array_map(
            fn (ThreadDto $dto) => ThreadResponse::createFromDto($dto),
            $this->threadFacade->getProfileThreadDtos(
                $profile,
                $query->offset ?? 0,
                $query->limit ?? 100
            )
        );
    }
}
