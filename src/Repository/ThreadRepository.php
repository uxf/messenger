<?php

declare(strict_types=1);

namespace UXF\Messenger\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\ProfileThreadLink;
use UXF\Messenger\Entity\Thread;

class ThreadRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * Help method for P2P messenger
     */
    public function findP2P(ProfileInterface $profileA, ProfileInterface $profileB): ?Thread
    {
        $threadId = $this->entityManager->createQueryBuilder()
                ->select('thread.id')
                ->from(Thread::class, 'thread')
                ->join(ProfileThreadLink::class, 'link', Join::WITH, 'link.thread = thread')
                ->where('link.profile IN (:profiles)')
                ->setParameter('profiles', [$profileA->getId(), $profileB->getId()])
                ->groupBy('thread.id')
                ->having('COUNT(link.profile) = 2')
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult()['id'] ?? null;

        return $threadId !== null ? $this->entityManager->find(Thread::class, $threadId) : null;
    }

    /**
     * @return Thread[]
     */
    public function findByProfile(ProfileInterface $profile, int $offset, int $limit): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('thread')
            ->from(Thread::class, 'thread')
            ->join(ProfileThreadLink::class, 'link', Join::WITH, 'link.thread = thread')
            ->where('link.profile = :profile')
            ->setParameter('profile', $profile)
            ->orderBy('thread.lastMessageAt', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }
}
