<?php

declare(strict_types=1);

namespace UXF\Messenger\Controller;

use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Http\Request\FromBody;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Facade\CreateMessageFacade;
use UXF\Messenger\Http\Request\MessageRequestBody;
use UXF\Messenger\Http\Response\MessageResponse;

class CreateMessageController
{
    public function __construct(
        private readonly CreateMessageFacade $messageFacade,
        private readonly CurrentProfileProviderInterface $currentProfileProvider,
    ) {
    }

    public function __invoke(Thread $thread, #[FromBody] MessageRequestBody $input, Request $request): MessageResponse
    {
        $file = $request->files->get('file');
        $author = $this->currentProfileProvider->getProfile();
        return MessageResponse::create($this->messageFacade->create($author, $thread, $input->text, $file));
    }
}
