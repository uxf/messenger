<?php

declare(strict_types=1);

namespace UXF\Messenger\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_messenger')]
class Thread
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id = 0;

    #[ORM\Column]
    private string $name;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $createdAt;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $lastMessageAt;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->createdAt = Clock::now();
        $this->lastMessageAt = Clock::now();
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getLastMessageAt(): DateTime
    {
        return $this->lastMessageAt;
    }

    public function setLastMessageAt(DateTime $lastMessageAt): void
    {
        $this->lastMessageAt = $lastMessageAt;
    }
}
