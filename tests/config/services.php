<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Doctrine\DefaultQuoteStrategy;
use UXF\Core\Shared\Repository\ProfileRepositoryInterface;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Core\Test\Client;
use UXF\MessengerTests\Entity\FakeProfile;
use UXF\MessengerTests\Mock\FakeCurrentProfileProvider;
use UXF\MessengerTests\Mock\FakeProfileRepository;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autoconfigure()
        ->autowire();

    $services->set(Client::class);

    $services->alias('test.client', Client::class);

    $services->load('UXF\MessengerTests\DataFixtures\\', __DIR__ . '/../DataFixtures');

    $services->set(DefaultQuoteStrategy::class);

    $services->alias('doctrine.orm.quote_strategy.default', DefaultQuoteStrategy::class);

    $services->load('UXF\MessengerTests\Mock\\', __DIR__ . '/../../tests/Mock');

    $services->alias(ProfileRepositoryInterface::class, FakeProfileRepository::class);

    $services->alias(CurrentProfileProviderInterface::class, FakeCurrentProfileProvider::class);

    $containerConfigurator->extension('framework', [
        'test' => true,
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore',
            'auto_mapping' => true,
            'mappings' => [
                'messenger_test' => [
                    'type' => 'attribute',
                    'dir' => '%kernel.project_dir%/tests/Entity',
                    'prefix' => 'UXF\MessengerTests\Entity',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('uxf_messenger', [
        'profile_class' => FakeProfile::class,
    ]);

    $containerConfigurator->extension('uxf_storage', [
        'upload_dir' => __DIR__ . '/../public/upload',
    ]);
};
