<?php

declare(strict_types=1);

namespace UXF\Messenger\Controller;

use UXF\Core\Http\Request\FromQuery;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Messenger\Entity\Message;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Http\Request\MessageRequestQuery;
use UXF\Messenger\Http\Response\MessageResponse;
use UXF\Messenger\Service\MessageService;

class GetMessagesController
{
    public function __construct(
        private readonly MessageService $messageService,
        private readonly CurrentProfileProviderInterface $currentProfileProvider,
    ) {
    }

    /**
     * @return MessageResponse[]
     */
    public function __invoke(Thread $thread, #[FromQuery] MessageRequestQuery $query): array
    {
        $profile = $this->currentProfileProvider->getProfile();

        return array_map(
            static fn (Message $message) => MessageResponse::create($message),
            $this->messageService->read($thread, $profile, $query->cursor),
        );
    }
}
