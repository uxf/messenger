<?php

declare(strict_types=1);

namespace UXF\Messenger\Controller;

use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Facade\ReadThreadFacade;
use UXF\Messenger\Http\Response\ThreadResponse;

class GetThreadController
{
    public function __construct(
        private readonly ReadThreadFacade $threadFacade,
        private readonly CurrentProfileProviderInterface $currentProfileProvider,
    ) {
    }

    public function __invoke(Thread $thread): ThreadResponse
    {
        $profile = $this->currentProfileProvider->getProfile();
        return ThreadResponse::createFromDto($this->threadFacade->getThreadDto($thread, $profile));
    }
}
