<?php

declare(strict_types=1);

namespace UXF\Messenger\Facade;

use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Dto\ThreadDto;
use UXF\Messenger\Entity\ProfileThreadLink;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Exception\ProfileNotInThreadException;
use UXF\Messenger\Http\Response\ProfileInteractionResponse;
use UXF\Messenger\Repository\MessageRepository;
use UXF\Messenger\Repository\ProfileThreadLinkRepository;
use UXF\Messenger\Repository\ThreadRepository;

class ReadThreadFacade
{
    public function __construct(
        private readonly ProfileThreadLinkRepository $linkRepository,
        private readonly ThreadRepository $threadRepository,
        private readonly MessageRepository $messageRepository,
    ) {
    }

    public function getThreadDto(Thread $thread, ProfileInterface $profile): ThreadDto
    {
        $profileThreadLink = null;

        $profileInteractions = array_map(static function (ProfileThreadLink $link) use ($profile, $thread, &$profileThreadLink) {
            if ($profile->getId() === $link->getProfile()->getId()) {
                $profileThreadLink = $link;
            }
            $msg = $link->getLastReadMessage();
            $id = "{$thread->getId()}_{$link->getProfile()->getId()}";
            return ProfileInteractionResponse::create($id, $link->getProfile(), $msg?->getId());
        }, $this->linkRepository->findByThread($thread));

        if (!$profileThreadLink instanceof ProfileThreadLink) {
            throw new ProfileNotInThreadException($profile, $thread);
        }

        $lastMessage = $this->messageRepository->findLastMessageInThread($thread);

        $unreadCount = $this->messageRepository->getUnreadMessageCountByThreadLink($profileThreadLink);

        return new ThreadDto($thread, $profileInteractions, $lastMessage, $unreadCount);
    }

    /**
     * @return ThreadDto[]
     */
    public function getProfileThreadDtos(ProfileInterface $profile, int $offset, int $limit): array
    {
        $result = [];

        foreach ($this->threadRepository->findByProfile($profile, $offset, $limit) as $thread) {
            $result[] = $this->getThreadDto($thread, $profile);
        }

        return $result;
    }
}
