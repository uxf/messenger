<?php

declare(strict_types=1);

namespace UXF\MessengerTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use UXF\MessengerTests\Entity\FakeProfile;

class MessengerDataFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $profileJohn = new FakeProfile(FakeProfile::JOHN_ID, 'John Doe');
        $manager->persist($profileJohn);

        $profileEmma = new FakeProfile(FakeProfile::EMMA_ID, 'Emma Watson');
        $manager->persist($profileEmma);

        $manager->flush();
    }
}
