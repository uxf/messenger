<?php

declare(strict_types=1);

namespace UXF\Messenger\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;
use UXF\Storage\Entity\File;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_messenger')]
class Message
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id = 0;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private ProfileInterface $author;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private Thread $thread;

    #[ORM\Column(length: 1000)]
    private string $text;

    #[ORM\ManyToOne]
    private ?File $file;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $createdAt;

    public function __construct(ProfileInterface $author, Thread $thread, string $text, ?File $file)
    {
        $this->author = $author;
        $this->thread = $thread;
        $this->text = $text;
        $this->file = $file;
        $this->createdAt = Clock::now();
        $this->thread->setLastMessageAt($this->createdAt);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuthor(): ProfileInterface
    {
        return $this->author;
    }

    public function getThread(): Thread
    {
        return $this->thread;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
