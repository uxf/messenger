<?php

declare(strict_types=1);

namespace UXF\Messenger\Repository;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Messenger\Entity\Message;
use UXF\Messenger\Entity\ProfileThreadLink;
use UXF\Messenger\Entity\Thread;

class MessageRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @return Message[]
     */
    public function findByThread(Thread $thread, ?int $cursorId, int $limit): array
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select(['message', 'author'])
            ->from(Message::class, 'message')
            ->join('message.author', 'author')
            ->where('message.thread = :thread')
            ->setParameter('thread', $thread)
            ->setMaxResults($limit)
            ->orderBy('message.id', 'DESC');

        if ($cursorId !== null) {
            $qb->andWhere('message.id < :cursor')
                ->setParameter('cursor', $cursorId);
        }

        return $qb->getQuery()->getResult();
    }

    public function findLastMessageInThread(Thread $thread): ?Message
    {
        return $this->entityManager->createQueryBuilder()
            ->select('message')
            ->from(Message::class, 'message')
            ->where('message.thread = :thread')
            ->setParameter('thread', $thread)
            ->setMaxResults(1)
            ->orderBy('message.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUnreadMessageCountByThreadLink(ProfileThreadLink $link): int
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('COUNT(message.id)')
            ->from(Message::class, 'message')
            ->where('message.thread = :thread')
            ->setParameter('thread', $link->getThread());

        if ($link->getLastReadMessage() !== null) {
            $qb->andWhere('message.id > :lastReadMessage')
                ->setParameter('lastReadMessage', $link->getLastReadMessage());
        }

        return (int) $qb->getQuery()->getSingleScalarResult();
    }
}
