<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Messenger\Controller\CreateMessageController;
use UXF\Messenger\Controller\CreateThreadController;
use UXF\Messenger\Controller\GetMessagesController;
use UXF\Messenger\Controller\GetThreadController;
use UXF\Messenger\Controller\GetThreadsController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('messenger_create_thread', '/api/messenger')
        ->controller(CreateThreadController::class)
        ->methods(['POST']);

    $routingConfigurator->add('messenger_get_threads', '/api/messenger')
        ->controller(GetThreadsController::class)
        ->methods(['GET']);

    $routingConfigurator->add('messenger_get_thread', '/api/messenger/{thread}')
        ->controller(GetThreadController::class)
        ->methods(['GET']);

    $routingConfigurator->add('messenger_create_message', '/api/messenger/{thread}/message')
        ->controller(CreateMessageController::class)
        ->methods(['POST']);

    $routingConfigurator->add('messenger_get_thread_messages', '/api/messenger/{thread}/message')
        ->controller(GetMessagesController::class)
        ->methods(['GET']);
};
