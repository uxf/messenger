<?php

declare(strict_types=1);

namespace UXF\Messenger\Controller;

use UXF\Core\Http\Request\FromBody;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;
use UXF\Messenger\Facade\CreateThreadFacade;
use UXF\Messenger\Facade\ReadThreadFacade;
use UXF\Messenger\Http\Request\ThreadRequestBody;
use UXF\Messenger\Http\Response\ThreadResponse;

class CreateThreadController
{
    public function __construct(
        private readonly CreateThreadFacade $createThreadFacade,
        private readonly ReadThreadFacade $readThreadFacade,
        private readonly CurrentProfileProviderInterface $currentProfileProvider,
    ) {
    }

    public function __invoke(#[FromBody] ThreadRequestBody $input): ThreadResponse
    {
        $profile = $this->currentProfileProvider->getProfile();
        $thread = $this->createThreadFacade->create($input, $profile);
        return ThreadResponse::createFromDto($this->readThreadFacade->getThreadDto($thread, $profile));
    }
}
