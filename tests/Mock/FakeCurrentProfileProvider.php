<?php

declare(strict_types=1);

namespace UXF\MessengerTests\Mock;

use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Core\Shared\Repository\ProfileRepositoryInterface;
use UXF\Core\Shared\Service\CurrentProfileProviderInterface;

class FakeCurrentProfileProvider implements CurrentProfileProviderInterface
{
    public function __construct(
        private readonly ProfileRepositoryInterface $profileRepository,
        public int $profileId = 0,
    ) {
    }

    public function getProfile(): ProfileInterface
    {
        $profile = $this->profileRepository->findProfile($this->profileId);
        if ($profile === null) {
            throw new \Exception();
        }
        return $profile;
    }
}
