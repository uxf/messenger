<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Response;

use UXF\Core\Shared\Entity\ProfileInterface;

final class ProfileResponse
{
    public function __construct(
        public readonly int $id,
        public readonly string $fullName,
    ) {
    }

    public static function create(ProfileInterface $profile): self
    {
        return new self(
            $profile->getId(),
            $profile->getFullName(),
        );
    }
}
