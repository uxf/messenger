<?php

declare(strict_types=1);

namespace UXF\MessengerTests\Mock;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Core\Shared\Repository\ProfileRepositoryInterface;
use UXF\MessengerTests\Entity\FakeProfile;

class FakeProfileRepository implements ProfileRepositoryInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function findProfile(int $id): ?FakeProfile
    {
        return $this->entityManager->find(FakeProfile::class, $id);
    }

    /**
     * @param int[] $ids
     * @return ProfileInterface[]
     */
    public function findProfiles(array $ids): array
    {
        if ($ids === []) {
            return [];
        }

        return $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(FakeProfile::class, 'p')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }
}
