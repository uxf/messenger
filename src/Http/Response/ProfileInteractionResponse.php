<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Response;

use UXF\Core\Shared\Entity\ProfileInterface;

final class ProfileInteractionResponse
{
    public function __construct(
        public readonly string $id,
        public readonly ProfileResponse $profile,
        public readonly ?int $lastReadMessageId,
    ) {
    }

    public static function create(string $id, ProfileInterface $profile, ?int $lastReadMessageId): self
    {
        return new self($id, ProfileResponse::create($profile), $lastReadMessageId);
    }
}
