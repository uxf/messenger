<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    $services->load('UXF\Messenger\Controller\\', __DIR__ . '/../src/Controller')
        ->public();

    $services->load('UXF\Messenger\Service\\', __DIR__ . '/../src/Service');

    $services->load('UXF\Messenger\Repository\\', __DIR__ . '/../src/Repository');

    $services->load('UXF\Messenger\Facade\\', __DIR__ . '/../src/Facade');
};
