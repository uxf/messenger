<?php

declare(strict_types=1);

namespace UXF\Messenger\Http\Request;

use UXF\Storage\Http\Request\FileRequestBody;

final class MessageRequestBody
{
    public function __construct(
        public readonly string $text,
        public readonly ?FileRequestBody $file = null,
    ) {
    }
}
