# UXF Messenger

## Install
```
$ composer req uxf/messenger
```

## Config

```php
// config/packages/uxf.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_messenger', [
        'profile_class' => Profile::class,
    ]);
};
```
