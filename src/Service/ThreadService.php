<?php

declare(strict_types=1);

namespace UXF\Messenger\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\ProfileThreadLink;
use UXF\Messenger\Entity\Thread;

class ThreadService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param ProfileInterface[] $profiles
     */
    public function create(string $name, array $profiles): Thread
    {
        $thread = new Thread($name);
        $this->entityManager->persist($thread);

        $uniqueProfiles = [];
        foreach ($profiles as $profile) {
            $uniqueProfiles[$profile->getId()] = $profile;
        }

        foreach ($uniqueProfiles as $profile) {
            $link = new ProfileThreadLink($thread, $profile);
            $this->entityManager->persist($link);
        }

        $this->entityManager->flush();
        return $thread;
    }
}
