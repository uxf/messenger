<?php

declare(strict_types=1);

namespace UXF\Messenger\Facade;

use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Core\Shared\Repository\ProfileRepositoryInterface;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Http\Request\ThreadRequestBody;
use UXF\Messenger\Service\ThreadService;

class CreateThreadFacade
{
    public function __construct(
        private readonly ThreadService $threadService,
        private readonly ProfileRepositoryInterface $profileRepository,
    ) {
    }

    public function create(ThreadRequestBody $input, ProfileInterface $creator): Thread
    {
        $profiles = $this->profileRepository->findProfiles($input->profiles);
        $profiles[] = $creator;

        return $this->threadService->create($input->name, $profiles);
    }
}
