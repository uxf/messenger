<?php

declare(strict_types=1);

namespace UXF\Messenger\Facade;

use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\Message;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Service\MessageService;
use UXF\Storage\Service\StorageFileService;

class CreateMessageFacade
{
    public function __construct(
        private readonly MessageService $messageService,
        private readonly StorageFileService $fileService,
    ) {
    }

    public function create(ProfileInterface $author, Thread $thread, string $text, mixed $uploadedFile): Message
    {
        $file = $uploadedFile !== null ? $this->fileService->createFile($uploadedFile, 'messenger') : null;
        return $this->messageService->create($author, $thread, $text, $file);
    }
}
