<?php

declare(strict_types=1);

namespace UXF\Messenger\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\Message;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Repository\MessageRepository;
use UXF\Messenger\Repository\ProfileThreadLinkRepository;
use UXF\Storage\Entity\File;

class MessageService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ProfileThreadLinkRepository $linkRepository,
        private readonly MessageRepository $messageRepository,
    ) {
    }

    public function create(ProfileInterface $author, Thread $thread, string $text, ?File $file): Message
    {
        $link = $this->linkRepository->getByThreadAndProfile($thread, $author);

        $message = new Message($author, $thread, $text, $file);
        $this->entityManager->persist($message);
        if ($file !== null) {
            $this->entityManager->persist($file);
        }
        $this->entityManager->flush(); // must generate message id

        if ($link->setLastReadMessage($message)) {
            $this->entityManager->flush();
        }

        return $message;
    }

    /**
     * @return Message[]
     */
    public function read(Thread $thread, ProfileInterface $profile, ?int $cursorId, int $limit = 50): array
    {
        $link = $this->linkRepository->getByThreadAndProfile($thread, $profile);

        $messages = $this->messageRepository->findByThread($thread, $cursorId, $limit);

        $lastMessage = $messages[0] ?? null;
        if ($lastMessage !== null && $link->setLastReadMessage($lastMessage)) {
            $this->entityManager->flush();
        }

        return $messages;
    }
}
