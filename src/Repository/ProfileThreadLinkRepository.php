<?php

declare(strict_types=1);

namespace UXF\Messenger\Repository;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\ProfileThreadLink;
use UXF\Messenger\Entity\Thread;
use UXF\Messenger\Exception\ProfileNotInThreadException;

class ProfileThreadLinkRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @return ProfileThreadLink[]
     */
    public function findByThread(Thread $thread): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('link')
            ->from(ProfileThreadLink::class, 'link')
            ->where('link.thread = :thread')
            ->setParameter('thread', $thread)
            ->orderBy('link.profile', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getByThreadAndProfile(Thread $thread, ProfileInterface $profile): ProfileThreadLink
    {
        $link = $this->entityManager->createQueryBuilder()
            ->select('link')
            ->from(ProfileThreadLink::class, 'link')
            ->where('link.thread = :thread')
            ->andWhere('link.profile = :profile')
            ->setParameter('thread', $thread)
            ->setParameter('profile', $profile)
            ->orderBy('link.profile', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();

        if (!$link instanceof ProfileThreadLink) {
            throw new ProfileNotInThreadException($profile, $thread);
        }

        return $link;
    }
}
