<?php

declare(strict_types=1);

namespace UXF\Messenger\Exception;

use Exception;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Messenger\Entity\Thread;

class ProfileNotInThreadException extends Exception
{
    public function __construct(ProfileInterface $profile, Thread $thread)
    {
        parent::__construct("Profile {$profile->getId()} is not in thread {$thread->getId()}");
    }
}
