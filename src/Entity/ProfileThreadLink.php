<?php

declare(strict_types=1);

namespace UXF\Messenger\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Shared\Entity\ProfileInterface;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_messenger')]
class ProfileThreadLink
{
    #[ORM\ManyToOne, ORM\Id]
    private Thread $thread;

    #[ORM\ManyToOne, ORM\Id]
    private ProfileInterface $profile;

    #[ORM\ManyToOne]
    private ?Message $lastReadMessage = null;

    #[ORM\Column(type: DateTime::class, nullable: true)]
    private ?DateTime $readAt = null;

    public function __construct(Thread $thread, ProfileInterface $profile)
    {
        $this->thread = $thread;
        $this->profile = $profile;
    }

    public function getThread(): Thread
    {
        return $this->thread;
    }

    public function getProfile(): ProfileInterface
    {
        return $this->profile;
    }

    public function getLastReadMessage(): ?Message
    {
        return $this->lastReadMessage;
    }

    public function getReadAt(): ?DateTime
    {
        return $this->readAt;
    }

    public function setLastReadMessage(Message $lastReadMessage): bool
    {
        if (
            $this->lastReadMessage === null ||
            $this->lastReadMessage->getId() < $lastReadMessage->getId()
        ) {
            $this->lastReadMessage = $lastReadMessage;
            $this->readAt = Clock::now();
            return true;
        }

        return false;
    }
}
